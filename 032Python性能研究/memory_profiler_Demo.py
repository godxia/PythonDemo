import random
import pandas
import numpy

from memory_profiler import profile
@profile
def run():
    x = [random.randint(25,150) for _ in range(1000)]
    pd = pandas.DataFrame({"x":x})
    pd.describe()

@profile
def run2():
    x = [random.randint(25,150) for _ in range(100000)]
    y = numpy.random.randint(25, 150, size=(100000))
    pd = pandas.DataFrame({"x":x})
    del x
    del y
    del pd


if __name__ == "__main__":
    run2()
