import requests
import pytest,os
def test_geoq_map_export_get():
    mapurl = "https://map.geoq.cn/arcgis/rest/services/ChinaOnlineCommunity/MapServer/export?bbox="
    XMin= -19655451.753
    YMin= -11127703.830
    XMax= 1.9554148991652016E7
    YMax= 2.0240971958386205E7
    url = f"{mapurl}{XMin},{YMin},{XMax},{YMax}&size=800,600&f=pjson"
    res = requests.get(url=url)
    assert res.status_code ==200
    js = res.json()
    assert "href" in js
    assert js["href"].find(".png") >=0




if __name__ == "__main__":
    pytest.main(["-vs","--alluredir=./allure",
    "./pytest_request_geoq.py"])
    os.system("allure serve ./allure")