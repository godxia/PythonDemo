import pytest
from web import DBFunction
import os
import random


def getyear():
    year = [y for y in range(1990,2030)]
    res = []
    for y in year:
        if y >=2019 or y <2000:
            res.append([y,False])
        else:
            res.append([y,True])
    return res

def getCode():
    code = [110000, 120000, 140000, 210000, 220000, 230000, 
    310000, 320000, 330000, 340000, 350000, 360000, 370000, 
    410000, 420000, 430000, 440000, 450000, 460000, 500000, 
    510000, 520000, 530000, 540000, 610000, 630000, 640000, 
    650000, 150000, 620000, 130000, 710000]

    res = []
    for c in code:
        res.append([c,True])
    
    for r in range(20):
        res.append([random.randint(0,100000),False])
    return res

def getCodeYear():
    code = getCode()
    year =getyear()
    res = []
    for i in range(100):
        c = random.choice(code)
        y = random.choice(year)
        res.append([c[0],y[0],c[1] and y[1]])
    return res
@pytest.mark.parametrize("param",getyear())
def test_web_DBFunction_DBFunc_T_CNGDP_getByCase1(param):
    res = DBFunction.DBFunc().T_CNGDP_getByCase(year=param[0])
    if not res["status"] : assert False
    assert res["result"] == param[1]

@pytest.mark.parametrize("param",getCode())
def test_web_DBFunction_DBFunc_T_CNGDP_getByCase2(param):
    res = DBFunction.DBFunc().T_CNGDP_getByCase(code=param[0])
    if not res["status"] : assert False
    assert res["result"] == param[1]

@pytest.mark.parametrize("param",getCodeYear())
def test_web_DBFunction_DBFunc_T_CNGDP_getByCase3(param):
    res = DBFunction.DBFunc().T_CNGDP_getByCase(code=param[0],year=param[1])
    if not res["status"] : assert False
    assert res["result"] == param[2]

if __name__ == "__main__":
    pytest.main(["-vs",
    "--clean-alluredir",
    "--alluredir=./allure",
    "./pytest_unit_03_codeDriver.py"])

    os.system("allure serve ./allure")