import allure
import pytest
import os
'''
allure.feature
可以定义不同的测试结论类型，在allure上面显示出来。
'''


@allure.feature('test_success')
@allure.description("这是一个成功的测试")
def test_success():
    """this test succeeds"""
    assert True


@allure.feature('test_failure')
@allure.description("这是一个失败的测试")
def test_failure():
    """this test fails"""
    assert False

@allure.feature('test_skip')
def test_skip():
    """this test is skipped"""
    pytest.skip('for a reason!')

@allure.feature('test_broken')
def test_broken():
    raise Exception('oops')

if __name__ == '__main__':
    # pytest.main(["-s","allure-test.py"])
    '''
    -q: 安静模式, 不输出环境信息
    -v: 丰富信息模式, 输出更详细的用例执行信息
    -s: 显示程序中的print/logging输出
    '''
    pytest.main(['-s', '-q','pytest_allure_01.py',
    '--clean-alluredir',
    '--alluredir=./allure'])
    os.system("allure serve ./allure")