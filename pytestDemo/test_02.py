import pytest
import logging
log = logging.getLogger(__name__)
@pytest.mark.dev
def test_001():
    print("开发测试1")
    log.info("开发测试1 - info")

@pytest.mark.dev
def test_002():
    print("开发测试2")
    log.debug("开发测试2 -debug")

@pytest.mark.run
def test_003():
    print("运行测试1")
    log.warning("运行测试1 - warning")

@pytest.mark.run
def test_004():
    print("运行测试2")
    log.error("运行测试2 -error")


if __name__ == "__main__":
    #pytest.main(["-s","-m","dev"])
    pytest.main(["-s","-m","dev or run"])
    #pytest.main(["-s","-m","not dev"])