import pytest,os
import pandas
import traceback
import requests,json

def read_xlsx(filename):
    try:
       df = pandas.read_excel(filename)
       return df.to_dict("records")
    except:
        traceback.print_exc()
        return []

@pytest.mark.parametrize("params",read_xlsx("./接口测试用例3.xlsx"))
class Test_interface(object):
    token = ""
    url = "http://localhost:5000/"

    #全局开始之前执行一次
    def setup_class(self):
        url = "http://localhost:5000/getToken"
        data = {"username": "admin",
                "password": "admin",
                "expiration":3600}
        res = requests.post(url,json=data).text
        self.token = json.loads(res)["token"]

    def test_if_01(self,params):
        ifName = params["name"]
        param = json.loads(params["params"])
        result = params["result"]
        if "token" in param:
            pass
        else:
            param["token"] = self.token
        
        url =f"{self.url}{ifName}" 
        res = json.loads(requests.post(url=url,json=param).text)
        assert res["status"]
        assert res["result"] == result

        

    def test_if_02(self,params):
        pass

if __name__ == '__main__':
    pytest.main(["-vs",
    "--clean-alluredir",
    "--alluredir=./allure",
    "./pytest_interface_dd_02.py"])

    os.system("allure serve ./allure")