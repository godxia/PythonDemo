from cmath import exp
import requests,json
import pytest,os
import yaml
import time

# @pytest.mark.parametrize("tk",yaml.load(open("./接口测试用例_1.yml").read(),Loader=yaml.SafeLoader))
# def test_getToken(tk):
#     url = "http://localhost:5000/getToken"
#     data = tk["param"]
#     res = requests.post(url,json=data)
#     print(res.status_code)
#     assert res.status_code == 200
#     rjs =  json.loads(res.text)
#     assert rjs["status"]
#     assert rjs["result"]== tk["result"]

def getToken():
    url = "http://localhost:5000/getToken"
    tk = []
    for e in (3600,5):
        data = {"username": "admin",
                "password": "admin",
                "expiration":e}
        res = requests.post(url,json=data).text
        token = json.loads(res)["token"]
        tk.append({"token":token,"expiration":e})
    return tk

# @pytest.mark.parametrize("tk",getToken())
# def test_getuser(tk):
#     url = "http://localhost:5000/getUser"
#     data = {"token":tk["token"]}
#     print(data)
#     result = True
#     if tk["expiration"] == 5:
#         time.sleep(5)
#         result = False

#     res = requests.post(url,json=data).text
#     print(res)
#     rjs = json.loads(res)
#     assert rjs["status"]
#     assert rjs["result"]== result


def getToken_Alltime():
    url = "http://localhost:5000/getToken"
    data = {"username": "admin",
            "password": "admin"}
    res = requests.post(url,json=data).text
    token = json.loads(res)["token"]
    return [token]

@pytest.mark.parametrize("tk",getToken_Alltime())
@pytest.mark.parametrize("data",json.load(open("./接口测试用例2.json")))
def test_getGDPByCase(tk,data):
    url = "http://localhost:5000/getGDPByCase"
    js = data["param"]
    js["token"] = tk
    res = json.loads(requests.post(url,json=js).text)
    assert res["status"]
    assert res["result"] == data["result"]

if __name__ == "__main__":
    pytest.main(["-vs",
    "--clean-alluredir",
    "--alluredir=./allure",
    "./pytest_interface_dd_01.py"])

    os.system("allure serve ./allure")
    