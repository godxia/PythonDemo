import pytest
import web.DBFunction

def test_web_DBFunction_DBFunc_T_USERS_getByName_01():
    name = "admin"
    res = web.DBFunction.DBFunc().T_USERS_getByName(name=name)
    if not res["status"] : assert False
    assert res["result"]

def test_web_DBFunction_DBFunc_T_USERS_getByName_02():
    name = "admin1"
    res = web.DBFunction.DBFunc().T_USERS_getByName(name=name)
    if not res["status"] : assert False
    assert res["result"] ==False

if __name__ == "__main__":
    pytest.main(["-vs","--alluredir=./allure",
    "./pytest_unit_01.py"])
