import pytest

class Test_01(object):
    def setup_class(self):
        print("全局变量，在执行前运行一次")
    
    def teardown_class(self):
        print("全局方法，在执行完成之后运行一次")
    
    def setup_method(self):
        print("每个方法开始之前都运行一次")

    def teardown_method(self):
        print("每个方法结束之后都运行一次")
    
    def test01(self):
        print("第一个测试方法")
        assert "a"=="a"

    def test02(self):
        print("第二个测试方法")
        assert "a"=="b"

def setup_module():
    print("每个模块开始前运行一遍")

def teardown_module():
    print("每个模块在结束时候运行一遍")

def setup_function():
    print("每个方法在运行前执行一遍")

def teardown_function():
    print("每个方法在结束时候执行一遍")

def test_01():
    print("A测试方法")
    assert 1==1

def test_02():
    print("B测试方法")
    assert 1==2


if __name__ == "__main__":
    pytest.main(["-s","./pytest_01.py"])
    #pytest.main(["-s"])