import flask,json
from flask_cors import CORS
import Function
import traceback

flaskApp = flask.Flask(__name__,static_url_path='')
CORS(flaskApp, supports_credentials=True)
from flasgger import Swagger
from flasgger.utils import swag_from
swagger_config = Swagger.DEFAULT_CONFIG
swagger_config['title'] = "测试Demo API"    # 配置大标题
swagger_config['description'] = "API库"     # 配置公共描述内容
Swagger(flaskApp)

###################################################
#
#   系统服务相关内容
#
@flaskApp.after_request
def after_request(response):
    '''
    HTTP请求头定义
    '''
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'x-requested-with,content-type')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,OPTIONS,DELETE')
    return response


@flaskApp.route("/")
def index():
    return flask.redirect('/apidocs')


@flaskApp.route("/getToken",methods=['POST'])
@swag_from('yaml/getToken.yml')
def getToken():
    '''
    获取token
    :param
    username 用户名
    password 密码
    expiration token时效时间
    :return:
    '''
    try:
        if "username" not in flask.request.json:
            return {"status":True,"result":False,"exception":"KeyError: 'username'"} 
        username = flask.request.json["username"]

        if "password" not in flask.request.json:
            return {"status":True,"result":False,"exception":"KeyError: 'password'"} 
        pwd = flask.request.json["password"]

        expiration = 3600
        if "expiration" in flask.request.json:
            expiration = flask.request.json["expiration"]
        return Function.getToken(username=username, pwd=pwd, expiration=expiration)
    except Exception as e:
        traceback.print_exc()
        return {"status":False,"exception":str(e)}

@flaskApp.route("/getUser",methods=['POST'])
@swag_from('yaml/getUser.yml')
def getUser():
    '''
    通过token获取用户信息，用于身份验证
    :param
    token : token
    :return:
    '''
    try:
        if "token" not in flask.request.json:return {"status":True,"result":False,"exception":"token not found"}
        token = flask.request.json["token"]
        return Function.getUserByToken(token=token)
    except Exception as e:
        return {"status":False,"exception":str(e)}

@flaskApp.route("/getGDPByCase",methods=['POST'])
@swag_from('yaml/getGDPByCase.yml')
def getGDPByCase():
    '''
    通过token获取用户信息，用于身份验证
    :param
    token : token
    :return:
    '''
    try:
        if "token" not in flask.request.json:return {"status":True,"result":False,"exception":"token not found"}
        token = flask.request.json["token"]
        code = None
        if "code" in flask.request.json:
            code = flask.request.json["code"]
            if isinstance(code,int):
                pass
            else:
                return {"status":True,"result":False,"exception":"code变量必须是整数"} 
        year = None
        if "year" in flask.request.json:
            year = flask.request.json["year"]
            if isinstance(year,int):
                pass
            else:
                return {"status":True,"result":False,"exception":"year变量必须是整数"} 
        return Function.getGDPByCase(token=token,code=code,year=year)
    except Exception as e:
        return {"status":False,"exception":str(e)}


if __name__ == "__main__":
    flaskApp.run("0.0.0.0",5000)