from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column,Integer,String,DateTime,Float,or_
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from datetime import datetime
import traceback
import web.util as util

db = "./web/test.db"
engine = create_engine('sqlite:///{0}?check_same_thread=False'.format(db), echo=False)
Base = declarative_base()

class T_USERS(Base):
    __tablename__ = 'users'
    id =  Column(Integer, primary_key=True)
    name = Column(String(255))
    password = Column(String(255))

    def to_dict(self):
        return {c.name: getattr(self, c.name, None) for c in self.__table__.columns}

class T_CNGDP(Base):
    __tablename__ = 'cngdp'
    index =  Column(Integer, primary_key=True)
    name= Column(String(255))
    code= Column(Integer)
    year= Column(Integer)
    value= Column(Float)

    def to_dict(self):
        return {c.name: getattr(self, c.name, None) for c in self.__table__.columns}

class DBFunc(object):
    def __init__(self):
        self.session_class = sessionmaker(bind=engine)
        self.session = self.session_class()

    def T_USERS_add(self,name,password):
        try:
            pwd = util.pwdToMd5(password)
            user = T_USERS(name= name,password= pwd)
            self.session.add(user)
            self.session.flush()
            self.session.commit()
            return {"Status":True,"Result":True,"newid":user.id}
        except Exception as e:
            traceback.print_exc()
            return {"Status": False,"msg":e}
    
    def T_USERS_getByName(self,name:str):
        try:
            user = self.session.query(T_USERS).filter(T_USERS.name == name).one_or_none()
            result = False
            if user is not None:
                user = user.to_dict()
                result = True

            return {"status":True,"result":result,"user":user}
        except Exception as e:
            traceback.print_exc()
            return {"status": False,"msg":e}
    
    def T_USERS_getById(self,id:int):
        try:
            user = self.session.query(T_USERS).filter(T_USERS.id == id).one_or_none()
            result = False
            if user is not None:
                user = user.to_dict()
                result = True

            return {"status":True,"result":result,"user":user}
        except Exception as e:
            traceback.print_exc()
            return {"status": False,"msg":e} 
    
    def T_CNGDP_getByCase(self,**kwargs):
        try:
            year,code = None,None
            if "year" in kwargs:
                year = kwargs["year"]
            
            if "code" in kwargs:
                code = kwargs["code"]

            query = self.session.query(T_CNGDP).filter(
                or_(T_CNGDP.year == year, year == None),
                or_(T_CNGDP.code == code, code == None)).all()

            result = False
            data = []
            res = [r.to_dict() for r in query]
            if len(res) >0:
                result = True
            return {"status":True,"result":result,"data":res}
            
        except Exception as e:
            traceback.print_exc()
            return {"status": False,"msg":e}
    

if __name__ == "__main__":
    res = DBFunc().T_CNGDP_getByCase(code=440000,year=2002)
    print(res["data"])
   