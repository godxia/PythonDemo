import json
import hashlib
import traceback
import time
from DBFunction import DBFunc
from util import *


def getToken(username: str, pwd: str, expiration=3600) -> dict:
    try:
        user = DBFunc().T_USERS_getByName(username)
        if not user["status"]:return user
        if not user["result"]:
            return {"status": True,"result":False, "msg": "登录失败，用户名不存在"}
        else:
            pwd = pwdToMd5(pwd)
            if user["user"]["password"] == pwd:
                id = user["user"]["id"]
            else:
                return {"status": True,"result":False, "msg": "登录失败，密码错误"}
        return generate_auth_token(id=id, expiration=expiration)
    except Exception as e:
        traceback.print_exc()
        return {"status": False, "msg": str(e)}

def getUserByToken(token:str)->dict:
    try:
        va = verify_auth_token(token=token)
        if not va["status"] or not va["result"]:return va
        res = DBFunc().T_USERS_getById(id = va["id"])
        del res["user"]["password"]
        return res

    except Exception as e:
        traceback.print_exc()
        return {"status": False, "msg": str(e)}

def getGDPByCase(token:str,code=None,year=None)->dict:
    try:
        va = verify_auth_token(token=token)
        if not va["status"] or not va["result"]:return va
        return DBFunc().T_CNGDP_getByCase(code=code,year=year)

    except Exception as e:
        traceback.print_exc()
        return {"status": False, "msg": str(e)}



