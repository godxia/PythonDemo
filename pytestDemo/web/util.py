import hashlib,authlib
import traceback
import time,json
from authlib.jose import JsonWebSignature

key = b"pytest"
def pwdToMd5(pwd: str) -> str:
    try:
        m = hashlib.md5("password".encode())
        m.update(pwd.encode())
        return m.hexdigest()
    except Exception as e:
        traceback.print_exc()

def generate_auth_token(id: int, expiration=600) -> dict:
    try:
        protected = {'alg': 'HS256'}
        payload = json.dumps(
            {"id": id, "timeExpired": int(time.time()+expiration)})
        x = JsonWebSignature().serialize_compact(
                protected, payload, key)
        return {"status":True,"result":True,"token": str(x, encoding='utf-8')}
    except Exception as e:
        traceback.print_exc()

        return {"status": False, "msg": e}
def verify_auth_token(token:str)->dict:
    try:
        if token =="" or token == None:return  {"status":True,"result":False,"msg":"token is None"}
        tk = JsonWebSignature().deserialize_compact(s=bytes(token, encoding = "utf8") ,key=key)
        data = json.loads(tk["payload"])
        if data["timeExpired"]<=time.time():
            return {"status":True,"result":False,"msg":"token timeout"}
        return {"status":True,"result":True,"id":data['id']}
    except authlib.jose.errors.DecodeError as ad:
        return {"status":True,"result":False,"msg":"token Error"}
    except:
        traceback.print_exc()
        return {"status":False,"msg":"token Error"}