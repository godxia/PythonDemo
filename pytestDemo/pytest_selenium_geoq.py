from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import pytest,os

param = [
    {
        "url" : "https://map.geoq.cn/ArcGIS/rest/services",
        "mapservice":"ChinaOnlineCommunity",
        "export_elm":"Export Map"
    }

    ,{
        "url" : "https://map.geoq.cn/ArcGIS/rest/services",
        "mapservice":"ChinaOnlineCommunity_123",
        "export_elm":"Export Map"
    }
]
@pytest.mark.parametrize("param",param)
def test_map_geoq_export_1(param):
    driver = webdriver.Chrome()
    driver.get(param["url"])
    mapservice = driver.find_element(By.LINK_TEXT,param["mapservice"])
    #assert mapservice.text == "ChinaOnlineCommunity"
    mapservice.click()
    export = driver.find_element(By.LINK_TEXT,param["export_elm"])
    #assert export.text == param["export_elm"]
    export.click()
    png = driver.find_element(By.TAG_NAME,"img")
    #print(png.get_attribute("src"))
    driver.get(png.get_attribute("src"))
    time.sleep(5)
    


if __name__ == '__main__':
    pytest.main(["-vs",
    "--clean-alluredir",
    "--alluredir=./allure",
    "./pytest_selenium_geoq.py"])

    os.system("allure serve ./allure")
    