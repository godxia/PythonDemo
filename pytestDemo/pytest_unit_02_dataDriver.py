import pytest
import web.DBFunction
import os
import pandas
import random
# @pytest.mark.parametrize装饰器，用于控制测试函数中输入的参数，
#语法如下：
# @pytest.mark.parametrize(反射参数名称:str,参数值:list)
#有多少个参数值，就会执行多少次
#一般情况下，参数值都是个二维list，因为需要同时输入参数和预期值。

# @pytest.mark.parametrize("a,b,result",[(1,2,3),(3,4,7)])
# class test_DDT_Demo(object):
#     def test_DDT_Demo_1(self,a,b,result):
#         assert a+b == result

#     def test_DDT_Demo_2(self,a,b,result):
#         assert a*b != result

#     def test_DDT_Demo_3(self,a,b,result):
#         assert a < result and b < result

#     def test_DDT_Demo_4(self,a,b,result):
#         assert result - b == a

# data_1 = [1, 2]
# data_2 = [3, 4, 5]
# @pytest.mark.parametrize('a', data_1)
# @pytest.mark.parametrize('b', data_2)
# def test_parametrize_1(a, b):
#     print(f'\n测试数据为{a}，{b}')


# data_1 = (
#     {
#         'user': 1,
#         'pwd': 2
#      },
#     {
#         'user': 3,
#         'pwd': 4
#     }
# )
# @pytest.mark.parametrize('dic', data_1)
# def test_parametrize_1(dic):
#     print(f'测试数据为{dic}')


# data1 = [
#     [1, 2, 3],
#     [2, 4, 6],
#     pytest.param(3, 4, 8, marks=pytest.mark.xfail),
#     pytest.param(3, 4, 7, marks=pytest.mark.skip)
# ]
# @pytest.mark.parametrize("a,b,result", data1)
# def test_mark(a, b, result):
#     print(f'测试数据为{a},{b}，结果为{result}')
#     assert a+b == result


#ids参数，标识用例
# data = [1, 2, 3]
# ids = [f'TestData-{a}' for a in data]

# @pytest.mark.parametrize('a', data ,ids= ids)
# def test_parametrize(a):
#     print(f'\n被加载测试数据为  {a}')


# data = [
# pytest.param(1, id="this is test1"),
# pytest.param(2, id="this is test2")
# ]

# @pytest.mark.parametrize('a', data)
# def test_parametrize(a):
#     print(f'\n被加载测试数据为  {a}')


# def getvalue():
#     return [random.randint(0,1000) for i in range(10)]

# @pytest.mark.parametrize('a', getvalue())
# def test_parametrize(a):
#     print(f'\n被加载测试数据为  {a}')

# test_data=[
#     {"id":1},
#     {"id":2},
#     {"id":3},
#     {"id":4},
# ]
# @pytest.fixture(params=test_data)
# def param_data(request):
#     return request.param

# def test_fixture(param_data):
#     print(f'\n被加载测试数据为  {param_data.get("id")}')


# @pytest.mark.parametrize("name",["admin","admin1","","xxx",1111])
# def test_web_DBFunction_DBFunc_T_USERS_getByName(name:str):
#     res = DBFunction.DBFunc().T_USERS_getByName(name=name)
#     assert res["status"]
#     assert res["user"] is not None

# @pytest.mark.parametrize("parm",[["admin",True],["admin1",False],["",False],["xxx",False],[111,False]])
# def test_web_DBFunction_DBFunc_T_USERS_getByName2(parm):
#     res = DBFunction.DBFunc().T_USERS_getByName(name=parm[0])
#     assert res["status"]
#     if parm[1]:
#         assert res["user"] is not None
#     else:
#         assert res["user"] is None

# # 参数可以分开映射
# @pytest.mark.parametrize("name,result",[["admin",True],["admin1",False],["",False],["xxx",False],[111,False]])
# def test_web_DBFunction_DBFunc_T_USERS_getByName3(name,result):
#     res = DBFunction.DBFunc().T_USERS_getByName(name=name)
#     assert res["status"]
#     if result:
#         assert res["user"] is not None
#     else:
#         assert res["user"] is None

def read_csv():
    df = pandas.read_csv("./用例.csv")
    return df.to_dict("records")

# 参数可用函数
@pytest.mark.parametrize("param",read_csv())
def test_web_DBFunction_DBFunc_T_USERS_getByName3(param):
    res = web.DBFunction.DBFunc().T_USERS_getByName(name=param["name"])
    assert res["status"]
    assert res["result"] == param["result"]


if __name__ == "__main__":
    pytest.main(["-vs",
    "--clean-alluredir",
    "--alluredir=./allure",
    "./pytest_unit_02_dataDriver.py"])

    os.system("allure serve ./allure")
    